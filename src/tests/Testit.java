package tests;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import timeUtils.TimeUtils;

class Testit {

    TimeUtils timeUtils = new TimeUtils();

    @Test
    @DisplayName("Testataan max rajakohta")
    void testMaxInt() {
	int aika = 0;
	String i = TimeUtils.secToTime(aika);

	while (i != "-1") {
	    aika++;
	    i = TimeUtils.secToTime(aika);
	}
	aika--;
	System.out.println("Maksimi rajakohta on: " + aika);
	assertTrue(i == "-1");
    }
    
    @Test
    @DisplayName("Testataan min rajakohta")
    void testMinInt() {
	int aika = 0;
	String i = TimeUtils.secToTime(aika);

	while (i != "-1") {
	    aika--;
	    i = TimeUtils.secToTime(aika);
	}
	aika++;
	System.out.println("Minimi rajakohta on: " + aika);
	assertTrue(i == "-1");
    }

    @DisplayName("Testataan kokonaisluku merkkijonoksi")
    @ParameterizedTest(name = "Luvun {0} muunnos merkkijonoksi {1}")
    @CsvSource({ "3665, 1:01:05", "10000, 2:46:40" })
    void testCorrectPrint(int number, String tulos) {
	String virhe = number + " on väärin";
	TimeUtils.secToTime(number);
	assertEquals(tulos, TimeUtils.secToTime(number), virhe);
    }

  
    
}